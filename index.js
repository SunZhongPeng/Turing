const http = require('http');
const url = require('url');
const fs = require('fs');
const req = require('request');

const _url = '/turing';

http.createServer(function (request, response) {
  let pathName = url.parse(request.url).pathname;
  let params = url.parse(request.url, true).query;

  if (pathName === _url) {
    pathName += '/index.html'
  }

  let is = isStaticRequest(pathName);
  if (is) {
    try {
      let data = fs.readFileSync('./page' + pathName.split(_url)[1]);
      response.writeHead(200);
      response.write(data);
      response.end();
    } catch (e) {
      response.writeHead(404);
      response.write('<html><body><h1>404 NotFound</h1></body></html>');
      response.end();
    }
  } else {
    if (pathName === _url + '/chat') {
      let data = {
        "reqType": 0,
        "perception": {
          "inputText": {
            "text": params.text
          }
        },
        "userInfo": {
          "apiKey": "bd289f5a7c45409ca04f9ac8fc310fdc",
          "userId": "123456"
        }
      };
      req({
        url: 'http://openapi.tuling123.com/openapi/api/v2',
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(data)
      }, function (error, resp, body) {
        if (!error && resp.statusCode === 200) {
          let obj = JSON.parse(body);
          if (obj && obj.results && obj.results.length > 0 && obj.results[0].values) {
            let head = {
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Methods': 'GET',
              'Access-Control-Allow-Headers': 'x-requested-with, content-type'
            };
            response.writeHead(200, head);
            response.write(JSON.stringify(obj.results[0].values));
            response.end()
          }
        } else {
          response.write('{\'text\':\'布吉岛你说的是什么\'}');
          response.end();
        }

      });
    }
  }
}).listen(9020);

function isStaticRequest(pathName) {
  let static = ['.html', '.css', '.js', 'jpg'];
  for (let i = 0; i < static.length; i++) {
    if (pathName.indexOf(static[i]) === pathName.length - static[i].length) {
      return true;
    }
  }
  return false;
}