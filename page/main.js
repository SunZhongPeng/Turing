function send(event) {
  if (event instanceof KeyboardEvent && event.key !== 'Enter') {
    return;
  }
  let val = document.getElementById('chatArea').value;
  if (val) {
    let me = document.createElement('p');
    me.style.textAlign = 'right';
    me.innerText = '我：' + val;
    document.getElementsByClassName('content')[0].appendChild(me);

    const ajax = new XMLHttpRequest();
    ajax.open('get', ('/turing/chat?text=' + val));
    ajax.send();
    ajax.onreadystatechange = function () {
      if (ajax.readyState === 4 && ajax.status === 200) {
        let temp = document.createElement('p');
        temp.innerText = 'Turing：' + JSON.parse(ajax.responseText).text;
        document.getElementsByClassName('content')[0].appendChild(temp);
      }
      document.getElementById('chatArea').value = '';
    }
  }
}

